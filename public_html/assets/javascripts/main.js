function result_success(result) {
	if(typeof result == 'object' && 'id' in result) {
		return result.id > 0;
	}
	return (typeof result == 'number' ? result > 0 : result == true);
}

function result_failure(result) {
	return !success(result);
}

function redirect(url) {
	window.location.href = url;
}

$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
	if(originalOptions.type === 'POST' && options.type === 'POST') {
		options.data = $.param($.extend(originalOptions.data, { _atok: (((new _td).getTime() / 1000 + REMOTE_TIME_DELAY - SERVER_START_TIME) * _tk / 1000 * (r = _tm.floor(_tm.random() * 100000))) + '-' + _tb + '-' + _tk + '-' + r }));
	}
});
