<?php

/**
 * application entry point
 */
 
error_reporting(E_ALL);

define('FPF_BASE_DIR', '../system/');
define('URL', $_SERVER['REQUEST_URI']);

define('FPF_CONFIG_DIR', FPF_BASE_DIR . 'config/');

define('FPF_PATH_INFO_KEY', 'fpf-path-info');

session_start();

require_once FPF_CONFIG_DIR . 'application.php';

require_once FPF_BASE_DIR . 'App.php';

$app = App::instance();
$app->launch();
$app->destroy();

?>