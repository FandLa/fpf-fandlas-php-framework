<?php

include_once FPF_DAOS_DIR . '/UserDAO.php';

class UserSession {
	
	private $currentUser;
	private $loginErrors = array();
	private $registrationErrors = array();
	
	public function setup() {
		$session = App::instance()->session;
		if(isset($session->userName) && isset($session->userEncPw)) {
			$this->currentUser = UserDAO::instance()->getUserByNameAndEncodedPassword($session->userName, $session->userEncPw);
			if($this->currentUser == NULL) {
				unset($session->userName);
				unset($session->userEncPw);
			}
		} else {
			$cookie = App::instance()->cookie;
			if(isset($cookie->userName) && isset($cookie->userEncPw)) {
				$session->userName = $cookie->userName;
				$session->userEncPw = $cookie->userEncPw;
				$this->setup();
			}
		}
	}
	
	public function login($name, $password, $setCookies = false) {
		$this->loginErrors = array();
		if(empty($name)) {
			$this->loginErrors[] = L_LOGIN_ERROR_NO_NAME;
		}
		if(empty($password)) {
			$this->loginErrors[] = L_LOGIN_ERROR_NO_PASSWORD;
		}
		if(count($this->loginErrors) != 0) {
			return false;
		}
		$encodedPassword = encode($password);
		if(($user = UserDAO::instance()->getUserByNameAndEncodedPassword($name, $encodedPassword)) != NULL) {
			$this->currentUser = $user;
			$session = App::instance()->session;
			$session->userName = $name;
			$session->userEncPw = $encodedPassword;
			if($setCookies) {
				$cookie = App::instance()->cookie;
				$cookie->userName = $name;
				$cookie->userEncPw = $encodedPassword;
			}
			return true;
		} else {
			if(UserDAO::instance()->getUserByName($name) == NULL) {
				$this->loginErrors[] = L_LOGIN_ERROR_INVALID_NAME;
			} else {
				$this->loginErrors[] = L_LOGIN_ERROR_WRONG_PASSWORD;
			}
		}
		return false;
	}
	
	public function getLoginErrors() {
		return $this->loginErrors;
	}
	
	public function registrate($name, $password, $confirmPassword) {
		$this->registrationErrors = array();
		if(empty($name)) {
			$this->registrationErrors[] = L_REGISTRATION_ERROR_NO_NAME;
		}
		if(empty($password)) {
			$this->registrationErrors[] = L_REGISTRATION_ERROR_NO_PASSWORD;
		}
		if(empty($confirmPassword)) {
			$this->registrationErrors[] = L_REGISTRATION_ERROR_NO_CONFIRM_PASSWORD;
		}
		if(count($this->registrationErrors) == 0) {
			if($password !== $confirmPassword) {
				$this->registrationErrors[] = L_REGISTRATION_ERROR_NO_MATCHING_PASSWORDS;
			} else {
				if(UserDAO::instance()->getUserByName($name) != NULL) {
					$this->registrationErrors[] = L_REGISTRATION_ERROR_NAME_TAKEN;
				} else {
					if($ru = UserDAO::instance()->insert($name, encode($password))) {
						return true;
					} else {
						$this->registrationErrors[] = L_REGISTRATION_ERROR_UNKNOWN;
					}
				}
			}
		}
		return false;
	}
	
	public function getRegistrationErrors() {
		return $this->registrationErrors;
	}
	
	public function logout() {
		if($this->currentUser != NULL) {
			$this->currentUser = NULL;
			$session = App::instance()->session;
			unset($session->userName);
			unset($session->userEncPw);
			$cookie = App::instance()->cookie;
			unset($cookie->userName);
			unset($cookie->userEncPw);
			return true;
		}
		return false;
	}
	
	public function getCurrentUser() {
		return $this->currentUser;
	}
	
}

?>