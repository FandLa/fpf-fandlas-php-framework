<?php

class Session {
	
	public function pop($key) {
		if(isset($this->$key)) {
			$pop_value = $this->$key;
			unset($this->$key);
			return $pop_value;
		}
		return NULL;
	}
	
	public function __set($key, $value) {
		$_SESSION[FPF_SESSION_VAR_PREFIX . $key] = $value;
	}
	
	public function &__get($key) {
		return $_SESSION[FPF_SESSION_VAR_PREFIX . $key];
	}
	
	public function __isset($key) {
		return isset($_SESSION[FPF_SESSION_VAR_PREFIX . $key]);
	}
	
	public function __unset($key) {
		unset($_SESSION[FPF_SESSION_VAR_PREFIX . $key]);
	}
	
}

?>