<?php

/**
 * Wrapper class for mysqli database connection
 */

class Database {
	
	private $pdo = NULL;
	
	public function setup() {
		$config = App::instance()->get_config();
		if(isset($config['PDO'])) {
			$database_config = $config['PDO'];
			$this->pdo = new PDO($database_config['type'] . ':host=' . $database_config['host'] . ';dbname=' . $database_config['dbname'], $database_config['user'], $database_config['password']);
		}
	}
	
	public function destroy() {
		$this->pdo = NULL;
	}
	
	public function query($query, $return_type = 'resource') {
		if($this->pdo == NULL) {
			throw new Exception('Not connected to the database');
		}
		$query_result = $this->pdo->query($query);
		switch($return_type) {
			case 'resource':
				return $query_result;
			case 'affected_rows': 
				return $this->pdo->affected_rows;
			case 'num_rows':
				return $query_result->num_rows;
			case 'insert_id':
				return $this->pdo->insert_id;
			case 'fetch_assoc':
				return (($assoc_row = $query_result->fetch_assoc()) ? $assoc_row : NULL);
			case 'fetch_assocs':
				$assoc_rows = array();
				while($assoc_row = $query_result->fetch_assoc()) {
					$assoc_rows[] = $assoc_row;
				}
				return $assoc_rows;
			case 'fetch_field':
				return (($field_row = $query_result->fetch_field()) ? $field_row : NULL);
			case 'fetch_fields':
				$field_rows = array();
				while($fields_row = $query_result->fetch_fields()) {
					$fields_rows[] = $fields_row;
				}
				return $fields_rows;
			case 'fetch_object':
				return (($object_row = $query_result->fetch_object()) ? $object_row : NULL);
			case 'fetch_objects':
				$object_rows = array();
				while($object_row = $query_result->fetch_object()) {
					$object_rows[] = $object_row;
				}
				return $object_rows;
		}
		return $query_result;
	}
	
	public function count($table, $additional_clause = '') {
		return $this->query('SELECT COUNT(0) "num_rows" FROM `' . mysql_escape($table) . '`' . ($additional_clause != '' ? ' ' . $additional_clause : ''), 'fetch_object')->num_rows;
	}

	public function __get($key) {
		return $this->pdo->$key;
	}
	
	public function __call($method, $arguments) {
		return call_user_func_array(array($this->pdo, $method), $arguments);
	}
	
	public function getPDO() {
		return $this->pdo;
	}
	
}

?>