<?php

class Dictionary {
	
	private $texts = NULL;
	
	public function setup() {
		if(!isset(App::instance()->session->locale)) {
			App::instance()->session->locale = FPF_DICTIONARY_DEFAULT_LOCALE;
		}
		$this->load_dictionary();
	}
	
	public function set_locale($locale) {
		App::instance()->session->locale = $locale;
		$this->load_dictionary();
	}
	
	public function get_locale() {
		return App::instance()->session->locale;
	}
	
	/**
	 * Loads the dictionary file located in /system/dictionary which is saved in .csv format (columns separated by ',' and lines separated by '\r\n')
	 * @param dictionary_file The dictionary file
	 */
	public function load_dictionary() {
		$this->texts = array();
		$dictionary_file = FPF_DICTIONARY_DIR . App::instance()->session->locale . '.' . FPF_DICTIONARY_FILE_EXTENSION;
		if(!file_exists($dictionary_file)) {
			throw new Exception('The dictionary file "' . $dictionary_file . '" does not exist');
		}
		$file_handle = fopen($dictionary_file, 'r');
		$positive_id = 10;
		$negative_id = -10;
		while(($columns = fgetcsv($file_handle, FPF_DICTIONARY_MAX_TEXT_LENGTH, ';')) !== false) {
			$constant_name = $columns[0];
			if(!empty($constant_name)) {
				if(strpos($constant_name, FPF_DICTIONARY_ERROR_TEXT_PREFIX) === 0) {
					$sign = '-';
					$constant_name = substr($constant_name, 1);
				} else {
					$sign = '+';
				}
				$constant_name = FPF_DICTIONARY_CONSTANT_NAME_PREFIX . $constant_name;
				if(!defined($constant_name)) {
					define($constant_name, ($sign == '+' ? $positive_id ++ : $negative_id --));
				}
				$this->texts[constant($constant_name)] = utf8_encode($columns[1]);
			}
		}
	}
	
	/**
	 * Interprets the text and parses the placeholders using the given arguments
	 */
	private function interpret_text($text, $arguments) {
		return preg_replace_callback('#{{(.*?)}}#', function($match) use ($arguments) {
			$interpreted_placeholder = preg_replace_callback('#\$(\w+)#', function($match) use($arguments) {
				return isset($arguments[$match[1]]) ? '\'' . $arguments[$match[1]] . '\'' : $match[0];
			}, $match[1]);
			return eval('return ' . $interpreted_placeholder . ';');
			// return $interpreted_placeholder == $match[1] ? '{{' . $match[1] . '}}' : eval('return ' . $interpreted_placeholder . ';');
		}, $text);
	}
	
	/**
	 * Returns the interpreted text of the given constant id/name
	 * @param text_id The constant id or the constant name for the given text
	 * @param arguments The arguments (key/value pairs) which replace the placeholders
	 * @return The interpreted text
	 */
	public function get_text($text_id, array $arguments = NULL) {
		if(!is_int($text_id)) {
			if(defined($text_id)) {
				$text_id = constant($text_id);
			} else {
				$constant_name_prefix = FPF_DICTIONARY_CONSTANT_NAME_PREFIX;
				if(defined($constant_name_prefix . $text_id)) {
					$text_id = constant($constant_name_prefix . $text_id);
				}
			}
		}
		if(is_int($text_id) && isset($this->texts[$text_id])) {
			return $this->interpret_text($this->texts[$text_id], $arguments);
		}
		return 'text{' . $text_id . '}';
	}
	
}

?>