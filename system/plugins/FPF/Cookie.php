<?php

class Cookie {
	
	public function pop($key) {
		if(isset($this->$key)) {
			$pop_value = $this->$key;
			unset($this->$key);
			return $pop_value;
		}
		return NULL;
	}
	
	public function __set($key, $value) {
		if($value == NULL) {
			setcookie(FPF_COOKIE_VAR_PREFIX . $key, $value, get_time() - 3600, '/');
		} else {
			setcookie(FPF_COOKIE_VAR_PREFIX . $key, $value, get_time() + FPF_COOKIE_EXPIRE, '/');
		}
	}
	
	public function __get($key) {
		return $_COOKIE[FPF_COOKIE_VAR_PREFIX . $key];
	}
	
	public function __isset($key) {
		return isset($_COOKIE[FPF_COOKIE_VAR_PREFIX . $key]);
	}
	
	public function __unset($key) {
		setcookie(FPF_COOKIE_VAR_PREFIX . $key, NULL, get_time() - 3600, '/');
	}
	
}

?>