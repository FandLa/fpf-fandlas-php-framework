<?php

class GlobalSettings {
	
	private $settings = array();
	
	public function setup() {
		if(!file_exists(FPF_CONFIG_DIR . 'global_settings.php')) {
			file_put_contents(FPF_CONFIG_DIR . 'global_settings.php', "<?php\nreturn array();\n?>");
		}
		$this->settings = include FPF_CONFIG_DIR . 'global_settings.php';
		if(!isset($this->settings) || !is_array($this->settings)) {
			$this->settings = array();
		}
	}
	
	public function destroy() {
		file_put_contents(FPF_CONFIG_DIR . 'global_settings.php', "<?php\nreturn " . var_export($this->settings, true) . ";\n?>");
	}
	
	public function __get($key) {
		return isset($this->settings[$key]) ? $this->settings[$key] : NULL;
	}
	
	public function __set($key, $value) {
		$this->settings[$key] = $value;
	}
	
	public function __isset($key) {
		return isset($this->settings[$key]);
	}
	
	public function __unset($key) {
		if(isset($this->settings[$key])) {
			unset($this->settings[$key]);
		}
	}
	
}

?>