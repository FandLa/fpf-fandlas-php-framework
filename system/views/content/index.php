<h2>Registered users</h2>
<?php

if(isset($this->session->editUserFeedback)) {
	if(isset($this->session->editUserId) && ($editUserFeedbackUser = UserDAO::instance()->getUserById($this->session->editUserId)) != NULL) {
		?>
		<div class="form-feedback">
			<div class="form-feedback-item" style="color: <?= result_success($this->session->editUserFeedback) ? 'green' : 'red' ?>;">
				<?= text($this->session->editUserFeedback, array('user_name' => $editUserFeedbackUser->getName())) ?>
			</div>
		</div>
		<?php
		unset($this->session->editUserId);
	}
	unset($this->session->editUserFeedback);
}


if(isset($this->session->deleteUserFeedback)) {
	if(isset($this->session->deleteUserName)) {
		?>
		<div class="form-feedback">
			<div class="form-feedback-item" style="color: <?= result_success($this->session->deleteUserFeedback) ? 'green' : 'red' ?>;">
				<?= text($this->session->deleteUserFeedback, array('user_name' => $this->session->deleteUserName)) ?>
			</div>
		</div>
		<?php
	}
	unset($this->session->deleteUserFeedback);
	unset($this->session->deleteUserName);
}


if(count($users) != 0) {
	?>
	<table border="1" cellspacing="0">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Encoded password</th>
			<th>Options</th>
		</tr>
		<?php
		foreach($users as $user) {
			if($editUser != NULL && $user->getId() == $editUser->getId()) {
				?>
				<form method="POST" action="<?= current_url() ?>">
					<input type="hidden" name="edit_user_id" value="<?= $user->getId() ?>" />
					<td><?= $user->getId() ?></td>
					<td><input style="width: 80px" type="text" name="edit_user_name" value="<?= $user->getName() ?>" placeholder="Enter name" /></td>
					<td><input type="password" name="edit_user_password" placeholder="Enter password" /></td>
					<td><input type="submit" name="edit_user_submit" value="Edit" /> <a href="<?= session_url(DO_PATH . 'delete-user', array('task' => 'delete', 'user-id' => $user->getId(), 'redirect' => URL)) ?>">Delete</a></td>
				</form>
				<?php
			} else {
				?>
				<tr>
					<td><?= $user->getId() ?></td>
					<td><?= $user->getName() ?></td>
					<td><?= $user->getEncodedPassword() ?></td>
					<td><a href="<?= current_url(array('task' => 'edit', 'user-id' => $user->getId())) ?>">Edit</a> <a href="<?= session_url(DO_PATH . 'delete-user', array('task' => 'delete', 'user-id' => $user->getId(), 'redirect' => URL))?>">Delete</a></td>
				</tr>
				<?php
			}
		}
		?>
	</table>
	<?php
} else {
?>
No users registered.
<?php
}
?>
