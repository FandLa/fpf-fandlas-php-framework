<h1><?= text(L_TITLE_REGISTRATION) ?></h1>

<?php
if(isset($this->session->registration_feedback) && count($this->session->registration_feedback) != 0) {
	?>
	<div class="form-feedback">
		<?php
		$registrationFeedback = $this->session->registration_feedback;
		foreach($registrationFeedback as $feedback) {
			?>
			<div class="form-feedback-item" style="color: <?= result_success($feedback) ? 'green' : 'red' ?>;">
				<?= $feedback == L_REGISTRATION_SUCCESS ? text($feedback, array('login_url' => url('/login'))) : text($feedback) ?>
			</div>
			<?php
		}
		unset($this->session->registration_feedback);
		?>
	</div>
	<?php
}
?>

<form method="POST" action="<?= URL ?>">
	<span class="form-label"><?= text(L_FORM_REGISTRATION_NAME) ?></span><input type="text" name="registration_name" placeholder="<?= text(L_FORM_REGISTRATION_ENTER_NAME) ?>" />
	<br />
	<span class="form-label"><?= text(L_FORM_REGISTRATION_PASSWORD) ?></span><input type="password" name="registration_password" placeholder="<?= text(L_FORM_REGISTRATION_ENTER_PASSWORD) ?>" />
	<br />
	<span class="form-label"><?= text(L_FORM_REGISTRATION_CONFIRM_PASSWORD) ?></span><input type="password" name="registration_confirm_password" placeholder="<?= text(L_FORM_REGISTRATION_ENTER_CONFIRM_PASSWORD) ?>" />
	<br />
	<span class="form-label">&nbsp;</span><input type="submit" name="registration_submit" value="<?= text(L_FORM_REGISTRATION_SIGN_UP) ?>"> <?= text(L_FORM_REGISTRATION_OR_LOGIN, array('login_url' => url('/login'))) ?>
</form>
