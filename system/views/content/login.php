<h1><?= text(L_TITLE_LOGIN) ?></h1>

<?php
if(isset($this->session->login_feedback) && count($this->session->login_feedback) != 0) {
	?>
	<div class="form-feedback">
		<?php
		$loginFeedback = $this->session->login_feedback;
		foreach($loginFeedback as $feedback) {
			?>
			<div class="form-feedback-item">
				<?= $feedback == L_LOGIN_ERROR_INVALID_NAME ? text($feedback, array('registration_url' => url('/registration'))) : text($feedback) ?>
			</div>
			<?php
		}
		unset($this->session->login_feedback);
		?>
	</div>
	<?php
}
?>

<form method="POST" action="<?= URL ?>">
	<span class="form-label"><?= text(L_FORM_LOGIN_NAME) ?></span><input type="text" name="login_name" placeholder="<?= text(L_FORM_LOGIN_ENTER_NAME) ?>" />
	<br />
	<span class="form-label"><?= text(L_FORM_LOGIN_PASSWORD) ?></span><input type="password" name="login_password" placeholder="<?= text(L_FORM_LOGIN_ENTER_PASSWORD) ?>" />
	<br />
	<span class="form-label"><?= text(L_FORM_LOGIN_STAY_LOGGED_IN) ?></span><input type="checkbox" name="login_remember" />
	<br />
	<span class="form-label">&nbsp;</span><input type="submit" name="login_submit" value="<?= text(L_FORM_LOGIN_LOGIN) ?>" /> <?= text(L_FORM_LOGIN_OR_SIGN_UP, array('registration_url' => url('/registration'))) ?>
</form>
