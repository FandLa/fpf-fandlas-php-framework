<?php
$show_pizza_url = new URL('Pizza');
$show_pizza_url->pizza = create_id_string($pizza->get_title(), $pizza->get_id());
?>

<tr>
	<td>
		<?= $pizza->get_id() ?>
	</td>
	<td>
		<a href="<?= $show_pizza_url ?>"><?= $pizza->get_title() ?></a>
	</td>
	<td>
		$<?= number_format($pizza->get_price(), 2) ?>
	</td>
</tr>