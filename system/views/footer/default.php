<?= text(L_CHANGE_LANGUAGE) ?>:

<?php
$locales = array(
	'en' => 'English',
	'de' => 'Deutsch'
);
$first = true;
foreach($locales as $locale => $locale_label) {
	$set_locale_url = session_url('/set-locale/' . $locale, array('redirect' => URL));
	$set_locale_link = '<a href="' . $set_locale_url . '">' . $locale_label .'</a>';
	if($this->dictionary->get_locale() == $locale) {
		$set_locale_link = '<b>' . $set_locale_link . '</b>';
	}
	if(!$first) {
		echo ' | ';
	}
	echo $set_locale_link;
	$first = false;
}
?>
