<!DOCTYPE html>
<html>
<head>
	
	<title><?= $this->get_page_title() ?></title>
	<meta name="description" content="<?= $this->get_page_description() ?>" />
	<meta name="keywords" content="<?= $this->get_page_keywords() ?>" />
	<meta name="author" content="<?= $this->get_page_author() ?>" />
	
	<meta charset="<?= $this->get_charset() ?>" />
	
	<!-- STYLESHEETS -->
	<?php 
	$this->include_stylesheets() 
	?>
	
	<script>
		URL = '<?= URL ?>';
		BASE_PATH = '<?= BASE_PATH ?>';
		STYLESHEETS_PATH = '<?= STYLESHEETS_PATH ?>';
		JAVASCRIPTS_PATH = '<?= JAVASCRIPTS_PATH ?>';
		IMAGES_PATH = '<?= IMAGES_PATH ?>';
		DO_PATH = '<?= DO_PATH ?>';
		AJAX_PATH = '<?= AJAX_PATH ?>';
		SERVER_START_TIME = <?= get_time() ?>;
		REMOTE_START_TIME = Math.floor(new Date().getTime() / 1000);
		REMOTE_TIME_DELAY = SERVER_START_TIME - REMOTE_START_TIME;
		var _tk = <?= ($token_base_key = intval(CSRF::generate_digit_hash(uniqid()))) ?>, _tb = <?= round((get_time() - FPF_CSRF_AJAX_TOKEN_ENCRYPT_KEY) / $token_base_key, 6) ?>, _td = Date, _tm = Math;
		<?php
		if(count(App::instance()->get_config('autoload', 'images')) > 0) {
			?>
			var autoload_images = new Array(<?php 
				$first_autoload_image = true;
				foreach(App::instance()->get_config('autoload', 'images') as $autoload_image) {
					echo ($first_autoload_image ? '' : ', ') . '\'' . $autoload_image . '\'';
					$first_autoload_image = false;
				}
			?>);
			var autoload_image_object = new Image();
			for(var i = 0; i < autoload_images.length; i ++) {
				autoload_image_object.src = '<?= IMAGES_PATH ?>' + autoload_images[i];
			}
			<?php
		}
	?>
	
	</script>
	
	<!-- JAVASCRIPTS -->
	<?php 
	$this->include_javascripts() 
	?>
	
</head>
<body>
	<?php
	$this->include_view('layout');
	?>
</body>
</html>