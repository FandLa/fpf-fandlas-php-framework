<?php
$currentUser = App::instance()->userSession->getCurrentUser();
if($currentUser == NULL) {
	?>
	<?= text(L_YOU_ARE_NOT_LOGGED_IN) ?>
	<?php
} else {
	?>
	Welcome, <?= $currentUser->getName() ?>
	<a href="<?= session_url('/logout') ?>" style="float: right">Logout</a>
	<?php
}
?>