
<!-- HEADER -->
<div id="header-container">
	<div id="header">
		<?php $this->include_view('header') ?>
	</div>
</div>

<!-- CONTENT -->
<div id="main-container">
	<div id="main">
		<?php $this->include_view('content') ?>
	</div>
</div>

<!-- FOOTER -->
<div id="footer-container">
	<div id="footer">
		<?php $this->include_view('footer') ?>
	</div>
</div>

