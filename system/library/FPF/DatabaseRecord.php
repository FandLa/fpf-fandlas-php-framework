<?php

/**
 * Used for database objects to make modifiying table values more comfortable
 */
class DatabaseRecord {
	
	private $table_name = NULL;
	
	private $primary_key_where_clause = NULL;
	
	protected $database = NULL;
	
	/**
	 * @param table_name The name of the database table.
	 * @param primary_key The primary key column of the table. For multiple keys, pass an associative array like ('col1' => 'val1', 'col2' => 'val2) - the second parameter then holds the primary key type(s) as value or array.
	 * @param primary_key_value The value of the primary key. Pass an array of primary key column types when having multiple primary keys.
	 * @param primary_key_type The type of the primary key column.
	 */
	protected function __construct($table_name, $primary_keys, $primary_key_value = NULL, $primary_key_type = 'text') {
		$this->table_name = mysql_escape($table_name); 
		if(!is_array($primary_keys)) {
			$primary_keys = array($primary_keys => $primary_key_value);
		} else {
			if($primary_key_value != NULL) {
				if(is_array($primary_key_value)) {
					$primary_key_types = $primary_key_value;
				} else {
					$primary_key_type = $primary_key_value;
				}
			}
		}
		if(isset($primary_key_types) && is_array($primary_key_types)) {
			$primary_key_type = reset($primary_key_types);
		}
		foreach($primary_keys as $primary_key => $primary_key_value) {
			if($this->primary_key_where_clause == NULL) {
				$this->primary_key_where_clause = ' WHERE ';
			} else {
				$this->primary_key_where_clause .= ' AND ';
			}
			switch($primary_key_type) {
				case 'text':
				default: 
				{
					$primary_key_value = '"' . mysql_escape($primary_key_value) . '"';
					break;
				}
				case 'date':
				{
					$primary_key_value = 'FROM_UNIXTIME(' . mysql_escape($primary_key_value) . ')';
					break;
				}
			}
			$this->primary_key_where_clause .= '`' . mysql_escape($primary_key) . '`=' . $primary_key_value . ' '; 
			if(isset($primary_key_types) && is_array($primary_key_types)) {
				$primary_key_type = next($primary_key_types);
			}
		} 
		$this->database = App::instance()->get_database();
	}
	
	/**
	 * @param update_columns The table column you want to update. To update multiple columns, pass an associative array('col1' => 'val1', 'col2' => 'val2') - the second parameter then holds the column type(s) as value or array.
	 * @param update_value The new value for the table column. Pass an array of column types when updating multiple columns.
	 * @param column_type The type of the column to update.
	 */
	protected function update_attribute($update_columns, $update_value = NULL, $column_type = 'text') {
		if(is_array($update_columns)) {
			$success = true;
			if($update_value != NULL) {
				if(is_array($update_value)) {
					$column_types = $update_value;
				} else {
					$column_type = $update_value;
				}
			}
			if(isset($column_types) && is_array($column_types)) {
				$column_type = reset($column_types);
			}
			foreach($update_columns as $update_column => $update_value) {
				if(!$this->update_attribute($update_column, $update_value, $column_type)) {
					$success = false;
				}
				if(isset($column_types) && is_array($column_types)) {
					$column_type = next($column_types);
				} 
			}
			return $success;
		} else {
			$query_string = 'UPDATE `' . $this->table_name . '` SET `' . mysql_escape($update_columns) . '`=';
			switch($column_type) {
				case 'text':
				default:
				{
					$query_string .= '"' . mysql_escape($update_value) . '"';
					break;
				}
				case 'date':
				{
					$query_string .= 'FROM_UNIXTIME(' . $update_value . ')';
					break;
				}
			}
			$query_string .= $this->primary_key_where_clause . ' LIMIT 1';
			$this->database->query($query_string);
			return $this->database->affected_rows == 1;
		}
	}
	
}

?>