<?php

/**
 * Container holding information about the url parameters (controller/action/param1/param2/...), url_path, url_segments, get and post
 * Passed to the controller's action
 */
class Request {
	
	public $controller_name = NULL;
	public $controller = NULL;
	public $action = NULL;
	
	public $url = NULL;
	public $path = NULL;
	
	public $get = NULL;
	public $post = NULL;
	
	public function __construct($controller_name, $action, $path) {
		$this->controller_name = $controller_name;
		$this->action = $action;
		$this->path = array_values(array_filter($path));
		$this->get = new Registry($_GET);
		unset($this->get->{'fpf-path-info'});
		$this->post = new Registry($_POST);
	}
	
}

?>