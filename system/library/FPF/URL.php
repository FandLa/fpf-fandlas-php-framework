<?php

class URL {
	
	public $scheme = 'http';
	public $host = '';
	public $path = '';
	public $parameters = array();
	public $isSessionUrl = false;
	
	public function __construct($url = '', $isSessionUrl = NULL, $parameters = NULL) {
		if(is_array($isSessionUrl)) {
			$parameters = $isSessionUrl;
			$isSessionUrl = NULL;
		}
		if(is_a($url, 'URL')) {
			$this->scheme = $url->scheme;
			$this->host = $url->host;
			$this->path = $url->path;
			if($parameters !== NULL) {
				$this->parameters = $parameters;
			} else {
				$this->parameters = $url->parameters;
			}
			if($isSessionUrl !== NULL) {
				$this->isSessionUrl = $isSessionUrl;
			} else {
				$this->isSessionUrl = $url->isSessionUrl;
			}
		} else {
			$this->parse($url, $isSessionUrl === NULL ? false : $isSessionUrl, $parameters === NULL ? array() : $parameters);
		}
	}
	
	private function parse($urlString, $isSessionUrl = false, $parameters) {
		$urlParts = parse_url($urlString);
		$this->scheme = isset($urlParts['scheme']) ? $urlParts['scheme'] : 'http';
		$this->host = isset($urlParts['host']) ? $urlParts['host'] . (isset($urlParts['port']) ? ':' . $urlParts['port'] : '') : $_SERVER['HTTP_HOST'];
		if(isset($urlParts['host'])) {
			if(isset($urlParts['path'])) {
				$this->path = $urlParts['path'];
			}
		} else {
			$this->path = BASE_PATH . ltrim($urlParts['path'], '/');
		}
		if(isset($urlParts['query'])) {
			parse_str($urlParts['query'], $this->parameters);
		}
		$this->parameters = array_merge($this->parameters, $parameters);
		$this->isSessionUrl = $isSessionUrl;
	}
	
	public function getScheme() {
		return $this->scheme;
	}
	
	public function getHost() {
		return $this->host;
	}
	
	public function getPath() {
		return $this->path;
	}
	
	public function getParameters() {
		return $this->parameters;
	}
	
	public function setParameters($parameters) {
		$this->parameters = is_array($parameters) ? $parameters : array();
	}
	
	public function addParameteres($parameters) {
		$this->parameters = array_merge($this->parameters, $parameters);
	}
	
	public function __set($key, $value) {
		$this->parameters[$key] = $value;
	}
	
	public function __get($key) {
		return $this->parameters[$key];
	}
	
	public function __isset($key) {
		return isset($this->parameters[$key]);
	}
	
	public function __unset($key) {
		unset($this->parameters[$key]);
	}
	
	public function setSessionUrl($isSessionUrl) {
		$this->isSessionUrl = $isSessionUrl;
	}
	
	public function isSessionUrl() {
		return $this->isSessionUrl;
	}
	
	public function getQueryString() {
		$first = true;
		$queryString = '';
		foreach($this->parameters as $parameter => $value) {
			if(!($this->isSessionUrl() && $parameter == '_stok')) {
				if(!$first) {
					$queryString .= '&';
				} else {
					$first = false;
				}
				$queryString .= $parameter . '=' . urlencode($value);
			}
		}
		return $queryString;
	}
	
	public function build() {
		$urlString = ($this->host == $_SERVER['HTTP_HOST'] ? '' : $this->scheme . '://' . $this->host) . '/' . ltrim($this->path, '/') . ($hasParameters = (count($this->parameters) > 0) ? '?' . $this->getQueryString() : '');
		if($this->isSessionUrl) {
			$urlString .= ($hasParameters ? '&' : '?') . '_stok=' . App::instance()->session->{FPF_CSRF_SESSION_TOKEN_KEY};
		}
		return $urlString;
	}
	
	public function __toString() {
		return $this->build();
	}
	
}

?>
