<?php

/**
 * Used to load libraries, scripts, DAOs, records
 */
class Loader {
	
	private function _include_file($file, $extension = '') {
		if(!is_file($file)) {
			$extension = '.php';
		}
		if(!file_exists($file . $extension)) {
			throw new Exception($file . ' not found');
		}
		require_once $file . $extension;
	}
	
	public function file($file) {
		$this->_include_file($file);
	}
	
	/**
	 * Loads the DAO and returns a new instance if $instantiate is true (by default)
	 * $dao_file The name of the DAO file (with or without extension)
	 * $dao_name optional The name of the DAO class. If unsupplied, the $dao_file will be used (folders will be assumed as namespaces) and the extension will be removed
	 * $instantiate optional Default is true. If true is passed, a new instance of the DAO class will be returned
	 */
	public function dao($dao_file, $dao_name = NULL, $instantiate = true) {
		$this->_include_file(FPF_DAOS_DIR . $dao_file);
		if($instantiate) {
			if($dao_name == NULL) {
				$dao_name = rtrim($dao_file, '.php');
			}
			$dao = new $dao_name();
			if(is_callable(array($dao, 'initialize'))) {
				$dao->initialize();
			}
			return $dao;
		}
	}
	
	/**
	 * Loads the library and returns a new instance if $instantiate is true
	 * $library_file The path of the library file
	 * $library_name optional The name of the library class. If unsupplied, the $library_file will be used (folders will be assumed as namespaces) and the extension will be removed
	 * $instantiate optional Default is true. If true is passed, a new instance of the library class will be returned
	 */
	public function library($library_file, $library_name = NULL, $instantiate = false) {
		$this->_include_file(FPF_LIBRARY_DIR . $library_file);
		if($instantiate) {
			if($library_name == NULL) {
				$library_class = rtrim($library_file, '.php');
				$library_folders = explode('/', $library_class);
				unset($library_folders[count($library_folders) - 1]);
				$library_namespace = implode('\\', $library_folders);
				$library_class = substr($library_class, ($last_slash_pos = strrpos($library_class, '/')) === false ? 0 : $last_slash_pos + 1);
				$library_name = ($library_namespace == '' ? '' : $library_namespace . '\\') . $library_class;
			}
			return new $library_name();
		}
	}
	
	/**
	 * Loads the plugin and returns a new instance
	 * $plugin_file The path of the plugin file
	 * $plugin_name optional The name of the plugin class. If unsupplied, the $plugin_file will be used (folders will be assumbed as namespaces) and the extension will be remove
	 * $instantiate optional Default is true. If true is passed, a new instance of the plugin class will be retuned
	 */
	public function plugin($plugin_file, $plugin_name = NULL, $instantiate = true) {
		$this->_include_file(FPF_PLUGINS_DIR . $plugin_file);
		if($instantiate) {
			if($plugin_name == NULL) {
				$plugin_class = rtrim($plugin_file, '.php');
				$plugin_folders = explode('/', $plugin_class);
				unset($plugin_folders[count($plugin_folders) - 1]);
				$plugin_namespace = implode('\\', $plugin_folders);
				$plugin_class = substr($plugin_class, ($last_slash_pos = strrpos($plugin_class, '/')) === false ? 0 : $last_slash_pos + 1);
				$plugin_name = $plugin_namespace . ($plugin_namespace == '' ? '' : '\\') . $plugin_class;
			}
			return new $plugin_name();
		}
	}
	
	public function record($record_file) {
		$this->_include_file(FPF_RECORDS_DIR . $record_file);
	}
	
	public function script($script) {
		$this->_include_file(FPF_SCRIPTS_DIR . $script);
	}
	
}

?>