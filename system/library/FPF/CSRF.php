<?php

define('FPF_TOKEN_PART_LENGTH', 8);

class CSRF {
	
	public static function setup() {
		$session = App::instance()->session;
		if(!isset($session->{FPF_CSRF_SESSION_TOKEN_KEY})) {
			$session->{FPF_CSRF_SESSION_TOKEN_KEY} = CSRF::generate_session_token();
		}
	}
	
	public static function generate_digit_hash($string) {
		$hash = substr(encode($string), 0, 10);
		$hash_length = strlen($hash);
		$digit_hash = $hash_length;
		for($i = 0; $i < $hash_length; $i ++) {
			$digit_hash = $digit_hash * 1.5 + strlen($digit_hash) * ord($hash[$i]);
		}
		return $digit_hash;
	}
	
	public static function generate_session_token() {
		return uniqid();
	}
	
	public static function validate_session_request($request) {
		if(!isset($request->url->_stok)) {
			return false;
		}
		return App::instance()->session->{FPF_CSRF_SESSION_TOKEN_KEY} === $request->url->_stok;
		/*
		$session_token_id = substr($session_token, 0, 5);
		$session_token_value = substr($session_token, 5);
		$expected_session_token_value = App::instance()->session->{FPF_CSRF_SESSION_TOKEN_ID_PREFIX . $session_token_id};
		if(isset($expected_session_token_value) && $expected_session_token_value === $session_token_value) {
			App::instance()->session->{FPF_CSRF_SESSION_TOKEN_ID_PREFIX . $session_token_id} = NULL;
			return true;
		}
		return false;
		 * 
		 */
	}
	
	public static function validate_ajax_request($request) {
		if(!isset($request->post->_atok)) {
			return false;
		}
		$token = $request->post->_atok;
		$token_parts = explode('-', $token);
		if(count($token_parts) != 4) {
			return false;
		}
		$telapsed = $token_parts[0];
		$tbase = $token_parts[1];
		$tbase_key = $token_parts[2];
		return abs(get_time() - (FPF_CSRF_AJAX_TOKEN_ENCRYPT_KEY + (($telapsed / ($tbase_key * $tbase_key / 1000 * $token_parts[3])) + $tbase) * $tbase_key)) < FPF_CSRF_AJAX_TOKEN_VALID_TIME;
	}
	
}

?>