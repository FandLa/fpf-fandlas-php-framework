<?php

class Registry {
	
	protected $values;
	
	public function __construct($values_array = NULL) {
		$this->values = array();
		if($values_array !== NULL && is_array($values_array)) {
			foreach($values_array as $key => $value) {
				$this->values[$key] = $value;
			}
		}
	}
	
	public function __set($key, $value) {
		$this->values[$key] = $value;
	}
	
	public function __get($key) {
		return isset($this->values[$key]) ? $this->values[$key] : NULL;
	}
	
	public function __isset($key) {
		return isset($this->values[$key]);
	}
	
	public function __unset($key) {
		unset($this->values[$key]);
	}
	
	public function get_values() {
		return $this->values;
	}
	
}

?>