<?php

class Singleton {
	
	protected static $singleton_instances = array();
	
	public function __construct() {
		
	}
	
	public function __clone() {
		
	}
	
	public static function instance() {
		$classname = get_called_class();
		if(!isset(static::$singleton_instances[$classname])) {
			static::$singleton_instances[$classname] = new static();
		}
		return static::$singleton_instances[$classname];
	}
	
}
