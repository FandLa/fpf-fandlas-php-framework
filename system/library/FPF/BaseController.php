<?php

/**
 * Base class for all controllers
 */
abstract class BaseController {
	
	protected $request = NULL;
	
	protected $page_title = 'FPF - FandLas PHP Framework';
	protected $page_description = 'FPF (FandLas PHP Framework) is a lightweight MVC oriented PHP framework.';
	protected $page_keywords = 'FPF, FandLas, PHP, Framework';
	protected $page_author = 'Stefan Fandler';
	
	protected $charset = 'UTF-8';
	
	protected $stylesheet_tags = NULL;
	protected $stylesheets_included = false;
	protected $javascript_tags = NULL;
	protected $javascripts_included = false;
	
	private $views = array(
		'html' => 'default.php',
		'layout' => 'default.php',
		'header' => 'default.php',
		'content' => NULL,
		'footer' => 'default.php'
	);
	
	public function __construct($request) {
		$this->request = $request;
	}
	
	public function requires_session_token() {
		return false;
	}
	
	public function on_invalid_session_token() {
		return false;
	}
	
	public function requires_ajax_token() {
		return false;
	}
	
	public function on_invalid_ajax_token() {
		return false;
	}
	
	public function check_tokens() {
		if($this->requires_session_token()) {
			if(!CSRF::validate_session_request($this->request)) {
				if(!$this->on_invalid_session_token()) {
					return false;
				}
			}
		}
		if($this->requires_ajax_token()) {
			if(!CSRF::validate_ajax_request($this->request)) {
				if(!$this->on_invalid_ajax_token()) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * This function is called before the action of the controller is invoked
	 */
	public function pre_process() {}
	
	/**
	 * This function is called after the display function of the controller is called
	 */
	public function post_process() {}
	
	/**
	 * Override this function to return an array of stylesheets to load
	 */
	public function load_stylesheets() {
		return array();
	}
	
	/**
	 * Override this function to return an array of javascripts to load
	 */
	public function load_javascripts() {
		return array();
	}
	
	public function initialize() {
		$this->load = new Loader($this);

		foreach(App::instance()->get_config('autoload', 'daos') as $dao) {
			if(!is_array($dao)) {
				$dao = array($dao);
			}
			$dao_file = $dao[0];
			$dao_alias = isset($dao[1]) ? $dao[1] : rtrim($dao[0], '.php');
			$dao_name = isset($dao[2]) ? $dao[2] : NULL;
			$this->$dao_alias = $this->load->dao($dao_file, $dao_name);
			App::instance()->process_object($this->$dao_alias);
		}
		$this->view = $this->load->library('FPF/Registry.php', 'Registry', true);
	}
	
	public function __get($key) {
		return App::instance()->{'get_' . $key}();
	}
	
	/**
	 * The default display() function shows the html page and its layout.
	 * By overriding this function, you can print your custom output to the page
	 */
	public function display() {
		if($this->stylesheet_tags === NULL) {
			$this->stylesheet_tags = '';
			$this->include_default_stylesheets();
		}
		foreach($this->load_stylesheets() as $stylesheet) {
			$this->include_stylesheet($stylesheet);
		}
		if($this->javascript_tags === NULL) {
			$this->javascript_tags = '';
			$this->include_default_javascripts();
		}
		foreach($this->load_javascripts() as $javascript) {
			$this->include_javascript($javascript);
		}
		$this->include_view('html');
	}
	
	protected function get_page_title() {
		return $this->page_title;
	}
	
	protected function set_page_title($page_title) {
		$this->page_title = $page_title;
	}
	
	protected function get_page_description() {
		return $this->page_description;
	}
	
	protected function set_page_description($page_description) {
		$this->page_description = $page_description;
	}
	
	protected function get_page_keywords() {
		return $this->page_keywords;
	}
	
	protected function set_page_keywords($page_keywords) {
		$this->page_keywords = $page_keywords;
	}
	
	protected function get_page_author() {
		return $this->page_author;
	}
	
	protected function set_page_author($page_author) {
		$this->page_author = $page_author;
	}
	
	protected function get_charset() {
		return $this->charset;
	}
	
	protected function set_charset($charset) {
		$this->charset = $charset;
	}
	
	protected function include_default_stylesheets() {
		foreach(App::instance()->get_config('autoload', 'stylesheets') as $default_stylesheet) {
			$this->include_stylesheet($default_stylesheet);
		}
	}
	
	protected function include_stylesheet($stylesheet) {
		$stylesheet_tag = '<link rel="stylesheet" type="text/css" href="' . STYLESHEETS_PATH . $stylesheet . '" />' . "\r\n";
		if($this->stylesheets_included) {
			echo $stylesheet_tag;
		} else {
			if($this->stylesheet_tags === NULL) {
				$this->stylesheet_tags = '';
				$this->include_default_stylesheets();
			}
			$this->stylesheet_tags .= $stylesheet_tag;
		}
	}
	
	protected function include_stylesheets() {
		echo $this->stylesheet_tags;
		$this->stylesheets_included = true;
	}
	
	protected function include_default_javascripts() {
		foreach(App::instance()->get_config('autoload', 'javascripts') as $default_javascript) {
			$this->include_javascript($default_javascript);
		}
	}
	
	protected function include_javascript($javascript) {
		$javascript_tag = '<script src="' . JAVASCRIPTS_PATH . $javascript . '"></script>' . "\r\n";
		if($this->javascripts_included) {
			echo $javascript_tag;
		} else {
			if($this->javascript_tags === NULL) {
				$this->javascript_tags = '';
				$this->include_default_javascripts();
			}
			$this->javascript_tags .= $javascript_tag;
		}
	}
	
	protected function include_javascripts() {
		echo $this->javascript_tags;
		$this->javascripts_included = true;
	}
	
	protected function set_view($view_name, $view) {
		$this->views[$view_name] = $view;
	}

	private function include_view_file($view_file, $view_variables = NULL, $view_variable_key = NULL) {
		if(!file_exists(FPF_VIEWS_DIR . $view_file)) {
			throw new Exception('View "' . $view_file . '" does not exist for ' . get_class($this));
		}
		$app = App::instance();
		foreach($this->view->get_values() as $key => $value) {
			$$key = $value;
		}
		if($view_variables != NULL) {
			if($view_variable_key != NULL) {
				$$view_variable_key = $view_variables;
			} else {
				foreach($view_variables as $view_variable_key => $view_variable_value) {
					$$view_variable_key = $view_variable_value;
				}
			}
		}
		include FPF_VIEWS_DIR . $view_file;
	}
	
	protected function include_view($view, $view_variables = NULL, $view_variable_key = NULL) {
		if(!isset($this->views[$view])) {
			throw new Exception('No view \'' . $view . '\' set for ' . get_class($this));
		}
		$this->include_view_file($view . '/' . $this->views[$view], $view_variables, $view_variable_key);
	}
	
	/**
	 * Includes the given template file and instanciates the given $template_variables as $variable_name => $variable_value
	 * @param template_file Path of the template file
	 * @param template_variables Template variables to use in the template
	 * @param template_variables_key Key of the template variables object
	 */
	protected function include_template($template_file, $template_variables = NULL, $template_variables_key = NULL) {
		$this->include_view_file('template/' . $template_file, $template_variables, $template_variables_key);
	}
	
	/**
	 * Includes the given template file for each $template_iterate_variables item. Iterates the template file with each $template_iterate_variables item
	 * Call examples:
	 * 	With template iterate variables:
	 * 		1:	('template.php', array('item' => array($a, $b, $c)))
	 * 		2:	('template.php', array($a, $b, $c), 'item')
	 * 	With template iterate variables and templates variables:
	 * 		3:	('template.php', array('item' => array($a, $b, $c)), array('var1' => 'val1', 'var2' => 'val2'))
	 * 		4:	('template.php', array($a, $b, $c), 'item', array('var1' => 'val1', 'var2' => 'val2'))
	 * 		5:	('template.php', array('item' => array($a, $b, $c)), 'var', 'key')
	 * 		6:	('template.php', array($a, $b, $c), 'item', 'var1', 'key1')
	 * 	With template iterate variables and callback function:
	 * 		7:	('template.php', array('item' => array($a, $b, $c)), function() {})
	 * 		8:	('template.php', array($a, $b, $c), 'item', function() {})
	 * 	With template iterate variables, template variables and callback function:
	 * 		9:	('template.php', array('item' => array($a, $b, $c)), array('var1' => 'val1', 'var2' => 'val2'), function() {})
	 * 		10:	('tepmlate.php', array($a, $b, $c), 'item', array('var1' => 'val1', 'var2' => 'val2'), function() {})
	 * 		11: ('template.php', array('item' => array($a, $b, $c)), 'var1', 'key1', function() {})
	 * 		12: ('template.php', array($a, $b, $c), 'item', 'var1', 'key1', function() {})
	 * @param template_file Path of the template file. Additional variable $iteration_count will be added which holds the iteration count, starting from 1
	 * @param template_iterate_variables Template variables array to iterate through
	 * @param template_iterate_variable_key
	 * @param template_variables Template variables to use in the template
	 * @param template_variables_key Key of the template variables object
	 * @param between_iteration_callback Callback function which will be called between each iteration. First parameter holds the iteration count, starting from 1
	 */
	protected function iterate_include_template($template_file, $template_iterate_variables, $template_iterate_variables_key = NULL, $template_variables = NULL, $template_variables_key = NULL, $between_iteration_callback = NULL) {
		if(!is_array($template_iterate_variables)) {
			throw new Exception('template_iterate_variables has to be of type array');
		} else {
			// case 1, 3, 9
			if($template_iterate_variables_key == NULL || is_array($template_iterate_variables_key)) {
				list($new_template_iterate_variables_key, $new_template_iterate_variables) = array_first_associative($template_iterate_variables);
				if(!is_array($new_template_iterate_variables) || !is_string($new_template_iterate_variables_key)) {
					throw new Exception('no template_iterate_variables_key or key via associative template_iterate_variables array given');
				}
				// case 3, 9
				if(is_array($template_iterate_variables_key)) {
					$between_iteration_callback = $template_variables_key;
					$template_variables_key = $template_variables;
					$template_variables = $template_iterate_variables_key;
				}
				$this->iterate_include_template($template_file, $new_template_iterate_variables, $new_template_iterate_variables_key, $template_variables, $template_variables_key, $between_iteration_callback);
			} else {
				// case 7
				if(is_callable($template_iterate_variables_key)) {
					$between_iteration_callback = $template_iterate_variables_key;
					$template_variables_key = NULL;
					$template_variables = NULL;
					$template_iterate_variables_key = NULL;
					$this->iterate_include_template($template_file, $template_iterate_variables, $template_iterate_variables_key, $template_variables, $template_variables_key, $between_iteration_callback);
				} else {
					// case 8
					if(is_callable($template_variables)) {
					 	$between_iteration_callback = $template_variables;
						$template_variables_key = NULL;
						$template_variables = NULL;
						$this->iterate_include_template($template_file, $template_iterate_variables, $template_iterate_variables_key, $template_variables, $template_variables_key, $between_iteration_callback);
					} else {
						// 9
						if(is_callable($template_variables_key)) {
					 		$between_iteration_callback = $template_variables_key;
							$template_variables_key = NULL;
							$this->iterate_include_template($template_file, $template_iterate_variables, $template_iterate_variables_key, $template_variables, $template_variables_key, $between_iteration_callback);
					 	} else {
					 		$this->_iterate_include_template($template_file, $template_iterate_variables, $template_iterate_variables_key, $template_variables, $template_variables_key, $between_iteration_callback);
					 	}
					 }
				}
			}
		}
	}

	private function _iterate_include_template($template_file, $template_iterate_variables, $template_iterate_variables_key, $template_variables, $template_variables_key, $between_iteration_callback) {
		if(!is_array($template_iterate_variables)) {
			throw new Exception('No array given as template_iterate_variables');
		}
		if(!is_string($template_iterate_variables_key)) {
			throw new Exception('No string given as template_iterate_variables_key');
		}
		$iteration_count = 1;
		foreach($template_iterate_variables as $template_iterate_variable_value) {
			if($between_iteration_callback != NULL && $iteration_count != 1) {
				$between_iteration_callback($iteration_count - 1);
			}
			$this->include_template($template_file, array_merge(array($template_iterate_variables_key => $template_iterate_variable_value), ($template_variables != NULL ? ($template_variables_key !== NULL ? array($template_variables_key => $template_variables) : $template_variables) : array()), array('iteration_count' => $iteration_count)));
			$iteration_count ++;
		}
	}

}

?>