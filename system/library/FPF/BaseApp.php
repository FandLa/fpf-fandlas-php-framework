<?php

include_once FPF_LIBRARY_DIR . '/FPF/Singleton.php';
include_once FPF_BASE_DIR . '/Controller.php';

abstract class BaseApp extends Singleton {
	
	/**
	 * Array of the application's configuration
	 */
	public $config = NULL;
	
	/**
	 * Loader used to load libraries, scripts, DAOs, records
	 */
	public $load = NULL;
	
	/**
	 * The current request to the site. Holding controller name, controller, action, url, parameters, get and post
	 */
	public $request = NULL;
	
	/**
	 * Path info for the current site (url)
	 */
	public $url_path = NULL;
	
	/**
	 * Array of objects which are setup after autoload is finished
	 */
	 private $setup_objects = array();
	
	/**
	 * Array of objects which are destroyed when the App is destroyed
	 */
	private $destroy_objects = array();
	
	public function launch() {
		$this->config = array();

		require_once FPF_LIBRARY_DIR . 'FPF/Loader.php';
		$this->load = new Loader($this);
		
		include_once FPF_CONFIG_DIR . 'config.php';
		
		include_once FPF_CONFIG_DIR . 'autoload.php';
		
		$this->autoload_libraries();
		$this->autoload_scripts();
		$this->autoload_plugins();
		$this->autoload_models();
		$this->setup_objects();
		
		include_once FPF_CONFIG_DIR . 'routes.php';
		
		$this->url_path = PLAIN_URL;
		$this->url_segments = array_filter(explode('/', ltrim($this->url_path, '/')));
		
		foreach($this->get_config('routes') as $route) {
			if(is_array($route)) {
				$route_pattern = $route[0];
				$route_callback = $route[1];
				if(preg_match_all($route_pattern, $this->url_path, $matches) > 0) {
					$matches_array = array();
					foreach($matches as $match) {
						$matches_array[] = $match[0];
					}
					if(($this->request = $route_callback($matches_array)) !== NULL && $this->request instanceof Request) {
						break;
					}
				}
			}
		}

		if(!isset($this->request)) {
			$this->request = new Request(FPF_ERROR_CONTROLLER . FPF_CONTROLLER_SUFFIX, FPF_DEFAULT_ACTION, array_filter(explode('/', ltrim($this->url_path, '/'))));
		}

		$get_parameters = $_GET;
		unset($get_parameters[FPF_PATH_INFO_KEY]);
		$this->request->url = new URL($this->url_path . '?' . http_build_query($get_parameters));
		
		// check the controller and action for correctness and possibly set back to default/error controller/action
		$this->prepare_request($this->request);
		
		// call pre process from (derived) App
		$this->pre_process($this->request);
		
		$this->request->controller->initialize();
		
		if($this->request->controller->check_tokens() !== false) {
			if($this->request->controller->pre_process($this->request) !== false) {
				// call the controller action
				if($this->request->controller->{FPF_CONTROLLER_ACTION_PREFIX . $this->request->action}($this->request) !== false) {
					// display the page
					$this->request->controller->display();
				}
				$this->request->controller->post_process($this->request);
			}
		}
		
		// call post process from (derived) App
		$this->post_process();
	}
	
	private function autoload_libraries() {
		foreach($this->get_config('autoload', 'libraries') as $library) {
			if(!is_array($library)) {
				$library = array($library);
			}
			$library_file = $library[0];
			$library_name = isset($library[1]) ? $library[1] : NULL;
			$this->load->library($library_file, $library_name);
			$library_class = rtrim($library_file, '.php');
			if(($last_slash_pos = strrpos($library_class, '/')) !== 0) {
				$library_class = substr($library_class, $last_slash_pos + 1);
			}
			$this->process_object($library_class);
		}
	}
	
	private function autoload_plugins() {
		foreach($this->get_config('autoload', 'plugins') as $plugin) {
			if(!is_array($plugin)) {
				$plugin = array($plugin);
			}
			$plugin_file = $plugin[0];
			$plugin_alias = $plugin[1];
			$plugin_name = isset($plugin[2]) ? $plugin[2] : NULL;
			$this->$plugin_alias = $this->load->plugin($plugin_file, $plugin_name);
			$this->process_object($this->$plugin_alias);
		}
	}
	
	private function autoload_models() {
		if($models_dir_handle = opendir(FPF_MODELS_DIR)) {
			while(($models_dir_file = readdir($models_dir_handle)) !== false) {
				if($models_dir_file != '.' && $models_dir_file != '..') {
					$models_dir_file = FPF_MODELS_DIR . $models_dir_file;
					if(is_dir($models_dir_file)) {
						$this->autoload_records($models_dir_file);
					} else {
						$this->load->file($models_dir_file);
					}
				}
			}
		}
	}

	private function autoload_scripts() {
		foreach($this->get_config('autoload', 'scripts') as $script) {
			$this->load->script($script);
		}
	}
	
	private function prepare_request() {
		if(!file_exists($controller_file = FPF_CONTROLLERS_DIR . $this->request->controller_name . '.php')) {
			$this->request->controller_name = FPF_ERROR_CONTROLLER;
			$this->request->action = FPF_DEFAULT_ACTION;
			if(!file_exists($controller_file = FPF_CONTROLLERS_DIR . $this->request->controller_name . '.php')) {
				throw new Exception('The default contoller ' . $this->request->controller_name . ' does not exist');
			}
		}
		require_once $controller_file;
		$this->request->controller = new $this->request->controller_name($this->request);
		if(!is_subclass_of($this->request->controller, 'Controller')) {
			throw new Exception('The controller ' . $this->request->controller_name . ' does not inherit from Controller');
		}
		if(!is_callable(array($this->request->controller, FPF_CONTROLLER_ACTION_PREFIX . $this->request->action))) {
			if(is_callable(array($this->request->controller, FPF_CONTROLLER_ACTION_PREFIX . FPF_ERROR_ACTION))) {
				$this->request->action = FPF_ERROR_ACTION;
			} else {
				if($this->request->controller_name != FPF_ERROR_CONTROLLER) {
					$this->request->controller_name = FPF_ERROR_CONTROLLER;
					$this->request->action = FPF_DEFAULT_ACTION;
					$this->prepare_request();
				} else {
					throw new Exception('No "' . $this->request->action . '" or "' . FPF_ERROR_ACTION . '"-action implemented in ' . $this->request->controller_name . '. ');
				}
			}
		}
	}

	public abstract function pre_process();
	
	public abstract function post_process();
	
	public function process_class($class) {
		
	}
	
	public function process_object($object) {
		if(is_callable(array($object, 'setup'))) {
			$this->setup_objects[] = $object;
		}
		if(is_callable(array($object, 'destroy'))) {
			$this->destroy_objects[] = $object;
		}
	}
	
	public function setup_objects() {
		foreach($this->setup_objects as $setup_object) {
			if(is_string($setup_object)) {
				$setup_object::setup();
			} else {
				$setup_object->setup();
			}
		}
		unset($this->setup_objects);
	}
	
	public function destroy_objects() {
		foreach($this->destroy_objects as $destroy_object) {
			if(is_string($destroy_object)) {
				$destroy_object::destroy();
			} else {
				$destroy_object->destroy();
			}
		}
		unset($this->destroy_objects);
	}

	public function destroy() {
		$this->destroy_objects();
	}
	
	public function get_config() {
		$return_config = $this->config;
		foreach(func_get_args() as $config_index) {
			$return_config = $return_config[$config_index];
		}
		return $return_config;
	}
	
	public function __call($name, $arguments) {
		if(strpos($name, 'get_') === 0) {
			$name = substr($name, strlen('get_'));
			if(isset($this->$name)) {
				return $this->$name;
			}
			throw new Exception('Undefined property: App::$' . $name);
		} else {
			if(strpos($name, 'get') === 0) {
				$name = lcfirst(substr($name, strlen('get')));
				if(isset($this->$name)) {
					return $this->$name;
				}
				throw new Exception('Undefined property: App::$' . $name);
			} else {
				throw new Exception('Call to undefined method App::' . $name . '()');
			}
		}
	}
	
}

?>