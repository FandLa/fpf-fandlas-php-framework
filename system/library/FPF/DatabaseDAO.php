<?php

abstract class DatabaseDAO extends Singleton {
	
	protected $database;
	
	public function __construct() {
		$this->database = App::instance()->getDatabase();
	}
	
	protected abstract function getTableName();
	
	protected abstract function assocToModel($row);
	
	protected function executeInsert($insertValues = array()) {
		$model = NULL;
		if($statement = $this->database->prepare('INSERT INTO `' . $this->getTableName() . '`(' . $this->createColumnClause($insertValues) . ') VALUES (' . $this->createInsertClause($insertValues) . ')')) {
			$this->bindStatementValues($statement, $insertValues);
			if($statement->execute()) {
				$insertValues['id'] = $this->database->lastInsertId();
				$model = $this->assocToModel($insertValues);
			}
			$statement = NULL;
		}
		return $model;
	}

	protected function executeUpdate($whereValues = array(), $updateValues = array()) {
		if($statement = $this->database->prepare('UPDATE `' . $this->getTableName() . '` SET ' . $this->createSetClause($updateValues) . ' WHERE ' . $this->createWhereClause($whereValues))) {
			$this->bindStatementValues($statement, $whereValues);
			$this->bindStatementValues($statement, $updateValues);
			if($statement->execute()) {
				return true;
			}
			$statement = NULL;
		}
		return false;
	}
	
	protected function executeDelete($whereValues = array()) {
		if($statement = $this->database->prepare('DELETE FROM `' . $this->getTableName() . '` WHERE ' . $this->createWhereClause($whereValues))) {
			$this->bindStatementValues($statement, $whereValues);
			if($statement->execute()) {
				return true;
			}
			$statement = NULL;
		}
		return false;
	}
	
	protected function executeGet($whereValues = array()) {
		$model = NULL;
		if($statement = $this->createSelectStatement($whereValues)) {
			if($row = $statement->fetch(PDO::FETCH_ASSOC)) {
				$model = $this->assocToModel($row);
			}
			$statement = NULL;
		}
		return $model;
	}

	protected function executeGetAll($whereValues = array(), $skip = 0, $limit = 999999999) {
		$models = NULL;
		if($statement = $this->createSelectStatement($whereValues, $skip, $limit)) {
			if($row = $statement->fetch(PDO::FETCH_ASSOC)) {
				$models = array();
				$models[] = $this->assocToModel($row);
			}
			while($row = $statement->fetch(PDO::FETCH_ASSOC)) {
				$models[] = $this->assocToModel($row);
			}
			$statement = NULL;
		}
		return $models;
	}
	
	private function bindStatementValues($statement, $values) {
		if(!is_array($values)) {
			return;
		}
		foreach($values as $column => $value) {
			$statement->bindValue(':' . $column, $value, is_int($value) ? PDO::PARAM_INT : PDO::PARAM_STR);
		}
	}
	
	private function createColumnClause($columnValues) {
		$columnClause = '';
		$first = true;
		foreach($columnValues as $column => $value) {
			if(!$first) {
				$columnClause .= ', ';
			}
			$first = false;
			$columnClause .= '`' . $column . '`';
		}
		return $columnClause;
	}
	
	private function createInsertClause($insertValues) {
		$insertClause = '';
		$first = true;
		foreach($insertValues as $column => $value) {
			if(!$first) {
				$insertClause .= ', ';
			}
			$first = false;
			$insertClause .= ':' . $column;
		}
		return $insertClause;
	}
	
	private function createSetClause($setValues) {
		$setClause = '';
		$first = true;
		foreach($setValues as $column => $value) {
			if(!$first) {
				$setClause .= ', ';
			}
			$first = false;
			$setClause .= '`' . $column . '`=:' . $column;
		}
		return $setClause;
	}
	
	private function createWhereClause($whereValues) {
		if($whereValues == NULL) {
			$whereValues = array();
		}
		$whereClause = '';
		foreach($whereValues as $column => $value) {
			$whereClause .= '`' . $column . '`=:' . $column . ' AND ';
		}
		return $whereClause . '1';
	}
	
	private function createSelectStatement($whereValues, $skip = 0, $limit = 999999999) {
		if($statement = $this->database->prepare('SELECT * FROM `' .  $this->getTableName(). '` WHERE ' . $this->createWhereClause($whereValues) . ' LIMIT :_skip, :_limit')) {
			$this->bindStatementValues($statement, $whereValues);
			$statement->bindValue(':_skip', $skip, PDO::PARAM_INT);
			$statement->bindValue(':_limit', $limit, PDO::PARAM_INT);
			if($statement->execute()) {
				return $statement;
			}
			$statement = NULL;
		}
		return NULL;
	}
	
}

?>
