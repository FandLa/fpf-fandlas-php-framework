<?php

/**
 * Returns the current timestmap
 * @return Current timestamp
 */
function get_time() {
	return time() + FPF_SERVER_TIME_DIFF;
}

/**
 * Returns an URL object
 * @param url The url string
 * @param is_session_url Wheter the url is a session url or not
 * @param parameters The parameters of the url
 * @return URL object
 */
function url($url, $is_session_url = false, $parameters = NULL) {
	return new URL($url, $is_session_url, $parameters);
}

/**
 * Returns an URL object and appens a session token to the query parameters
 * @param url The url string or an URL object
 * @param parameters The parameters of the url
 * @return URL object
 */
function session_url($url, $parameters = NULL) {
	return new URL($url, true, $parameters);
}

/**
 * Returns an URL object from the current plain url (without parameters or fragments)
 * @param is_session_url Wheter the url is a session url or not
 * @param parameters The parameters of the url
 * @return URL object
 */
function current_url($is_session_url = false, $parameters = NULL) {
	return new URL(PLAIN_URL, $is_session_url, $parameters);
}
 
/**
 * Redirects to url relative to base path.
 * @param url Url to redirect to. Can be an object or a string
 */
function redirect($url = '') {
	redirect_absolute(is_a($url, 'URL') ? $url : url($url));
}

/**
 * Redirects to absolute url.
 * @param url Url to redirect to. Can be an object or a string
 */
function redirect_absolute($url = '') {
	App::instance()->destroy();
	if(!headers_sent()) {
		header('Location: ' . $url);
		exit;
	} else {
		throw new Exception('Cannot redirect: Headers already sent');
	}
}

/**
 * Redirects to current url. (Refreshes the site)
 */
function redirect_current() {
	redirect_absolute(URL);
}

/**
 * Logs error message into error.log
 * @param message The error message
 */
function error($message) {
	ob_start();
	debug_print_backtrace();
	$backtrace = ob_get_contents();
	ob_end_clean();
	file_put_contents('error.log', date('[d.m.Y H:i:s]', get_time()) . ': ' . $message . "\r\n" . $backtrace . "\r\n", FILE_APPEND);
}

/**
 * Encodes value into sha1 hash
 * @param value The value/string to be encoded
 * @return sha1 hash
 */
function encode($value) {
	return sha1($value);
}

/**
 * Encodes value into md5 hash and removes all characters but digits
 * @param value The value/string to be encoded
 * @return md5 digit hash, consisting of digits only
 */
function encode_digits($value) {
	return preg_replace('#[^0-9]#', '', encode($value));
}

/**
 * Escapes mysql query string to avoid mysql injection
 * @param string The string to escape
 * @return The escaped string
 */
function mysql_escape($string) {
	return App::instance()->getDatabase()->get_connection()->real_escape_string($string);
}

/**
 * Returns the text_id's text in the current language using the App's dictionary
 * @param text_id The id of the text or the name of the text constant
 * @param arguments Associative array of arguments used in the text's placeholders
 * @return The text
 */
function text($text_id, array $arguments = NULL) {
	return App::instance()->getDictionary()->get_text($text_id, $arguments);
}

/**
 * Returns the result as associative array holding the text's id and the text.
 * The return of result_text() can also be passed to success() and failure() to
 * see wheter the result is a success or failure
 * @param text_id The id of the text or the name of the text constant
 * @param arguments Associative array of arguments used in the text's placeholders
 * @return The result
 */
function result_text($text_id, array $arguments = NULL) {
	return array(
		'id' => is_numeric($text_id) ? $text_id : (defined($text_id) ? constant($text_id) : $text_id),
		'text' => text($text_id, $arguments)
	);
}

/**
 * Checks if the result is a success or failure
 * Positive numbers are success, negative numbers are failures
 * @param result Result string (constant), id, number or array containing the result/id
 * @return True if the result is a success, false if not
 */
function result_success($result) {
	if(is_array($result)) {
		return $result['id'] > 0;
	}
	return (is_numeric($result) ? $result > 0 : (is_bool($result) ? $result : (defined($result) ? constant($result) > 0 : false)));
}

/**
 * Opposite of result_success(). Checks if the result is a success or failure
 * Positive numbers are success, negative numbers are failures
 * @param result Result string (constant), id, number or array with an 'id' field, which holds the result
 * @return True if the result is a failure, false if not
 */
function result_failure($result) {
	return !success($result);
}

/**
 * Converts value and key/id into url string used to indicate records in the url
 * Stefan-Fandler-19	 <firstname>-<surname>-<id>
 * @param text The text of the url string
 * @param id The id of the url string
 * @return Url string
 */
function create_id_string($text, $id) {
	return preg_replace('#[^a-z0-9äöüß\-]#', '-', strtolower($text)) . '-' . $id;
}

/**
 * Parses the key/id from the url string
 * @param url_string Url string
 * @return Key/id parsed from the url string 
 */
function parse_id_string($url_string) {
	return (($last_hyphen_pos = strrpos($url_string, '-')) === false ? NULL : substr($url_string, $last_hyphen_pos + 1, strlen($url_string) - $last_hyphen_pos - 1));
}

/**
 * Get the first value of an array
 * @param array Array to get the value from
 * @return First value of array
 */
function array_first($array) {
	foreach($array as $key => $value) {
		return $value;
	}
}

/**
 * Get the first key of an array
 * @param array Array to get the key from
 * @return First key of array
 */
function array_first_key($array) {
	foreach($array as $key => $value) {
		return $key;
	}
}

/**
 * Get the first key/value of an array
 * @param arary Array to get the key/value from
 * @return Array holding key, value of first element
 */
function array_first_associative($array) {
	foreach($array as $key => $value) {
		return array($key, $value);
	}
}

?>