<?php

include_once FPF_LIBRARY_DIR . '/FPF/BaseController.php';

class Controller extends BaseController {
	
	public function requires_logged_user() {
		return true;
	}
	
	public function on_unlogged_user() {
		redirect('/login');
		return false;
	}
	
}

?>