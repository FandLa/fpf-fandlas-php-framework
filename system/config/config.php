<?php

/**
 * PDO (PHP Data Object) configuration (only mysql supported)
 */
$this->config['PDO'] = array(
	'type' => 'mysql',
	'host' => 'localhost',
	'user' => 'root',
	'password' => '',
	'dbname' => 'game'
);

?>