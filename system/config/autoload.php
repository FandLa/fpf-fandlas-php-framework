<?php

$this->config['autoload'] = array();

/**
 * Stylesheets to load by default
 * An absolute path is required
 */
$this->config['autoload']['stylesheets'] = array(
	'layout.css'
);

/**
 * Javascripts to load by default
 * An absolute path is required
 */
$this->config['autoload']['javascripts'] = array(
	'jquery.js',
	'main.js'
);

/**
 * Images to load by default
 * An absolute path is required
 */
$this->config['autoload']['images'] = array(
	
);

 /**
  * Libraries to load
  * (<library-class-name>, <library-file [optional]>)
  * 
  * The following library descriptor will load the library class named 'DatabaseRecord' from 'MyDatabaseRecord.php'
  * in the /system/library directory:
  * array('DatabaseRecord', 'MyDatabaseRecord.php')
  */
$this->config['autoload']['libraries'] = array(
	'FPF/Request',
	'FPF/Registry',
	'FPF/CSRF',
	'FPF/DatabaseDAO',
	'FPF/DatabaseRecord',
	'FPF/URL'
);

/**
 * DAOs to load (available in Controller only)
 * (<dao-class-name>, <dao-instance-name>, <dao-file [optional]>)
 * 
 * The following DAO descriptor will load the DAO class named 'Car' from 'MyCars.php' located 
 * in the /system/daos directory and will create an DAO-instance called 'cars' in the controller:
 * array('Car', 'cars', 'MyCars.php)
 */
$this->config['autoload']['daos'] = array(
	array('UserDAO')
);

/**
 * Plugins to load (available in both Controller and App)
 * (<plugin-class-name>, <plugin-instance-name>, <plugin-file [optional]>)
 * 
 * The following plugin descriptor will load the plugin class named 'Database' from 'MyDatabase.php' located
 * in the /system/plugins directory and will create an instance called 'database' in the controller:
 * array('Database', 'database', 'MyDatabase.php')
 */
$this->config['autoload']['plugins'] = array(
	array('FPF/Session', 'session', 'Session'),
	array('FPF/Cookie', 'cookie', 'Cookie'),
	array('FPF/Database', 'database', 'Database'),
	array('FPF/Dictionary', 'dictionary', 'Dictionary'),
	array('FPF/GlobalSettings', 'globalSettings', 'GlobalSettings'),
	array('UserSession', 'userSession', 'UserSession')
);

/**
 * Your custom scripts to be loaded by default
 * The file extension has to be supplied
 */
$this->config['autoload']['scripts'] = array(
	'helpers.php'
);

?>