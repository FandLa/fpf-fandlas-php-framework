<?php

/**
 * Controller configuration
 */
define('FPF_CONTROLLER_SUFFIX', 'Controller');
define('FPF_CONTROLLER_ACTION_PREFIX', 'action_');

define('FPF_DEFAULT_CONTROLLER', 'Index' . FPF_CONTROLLER_SUFFIX);
define('FPF_DEFAULT_ACTION', 'index');

define('FPF_ERROR_CONTROLLER', 'Error' . FPF_CONTROLLER_SUFFIX);
define('FPF_ERROR_ACTION', 'undefined');

/**
 * Session and Cookie configuration
 */
define('FPF_SESSION_VAR_PREFIX', 'fpf-ss-');
define('FPF_COOKIE_VAR_PREFIX', 'fpf-ck-');
define('FPF_COOKIE_EXPIRE', 60 * 60 * 24 * 365);

/**
 * Time configuration
 */
define('FPF_SERVER_TIME_DIFF', -60 * 60);

/**
 * CSRF session and ajax token configuration
 */
define('FPF_CSRF_SESSION_TOKEN_KEY', 'stok');
define('FPF_CSRF_AJAX_TOKEN_ENCRYPT_KEY', 'hJqw6TVW');
define('FPF_CSRF_AJAX_TOKEN_VALID_TIME', 5);

/**
 * Dictionary and locale configuration
 */
define('FPF_DICTIONARY_DIR', FPF_BASE_DIR . 'dictionary/');
define('FPF_DICTIONARY_DEFAULT_LOCALE', 'en');
define('FPF_DICTIONARY_FILE_EXTENSION', 'csv');
define('FPF_DICTIONARY_CONSTANT_NAME_PREFIX', 'L_');
define('FPF_DICTIONARY_ERROR_TEXT_PREFIX', '!');
define('FPF_DICTIONARY_MAX_TEXT_LENGTH', 1000);

/**
 * Path and directory configuration
 */
// WWW
define('PLAIN_URL', '/' . (!empty($_GET[FPF_PATH_INFO_KEY]) ? $_GET[FPF_PATH_INFO_KEY] : ''));
define('BASE_PATH', dirname($_SERVER['SCRIPT_NAME']) . '/');
/*
define('LOCAL_ASSETS_PATH', 'assets/');
define('LOCAL_STYLESHEETS_PATH', LOCAL_ASSETS_PATH . 'stylesheets/');
define('LOCAL_JAVASCRIPTS_PATH', LOCAL_ASSETS_PATH . 'javascripts/');
define('LOCAL_IMAGES_PATH', LOCAL_ASSETS_PATH . 'images/');
define('LOCAL_DO_PATH', 'do/');
define('LOCAL_AJAX_PATH', 'ajax/');
*/
define('ASSETS_PATH', 'assets/');
define('STYLESHEETS_PATH', ASSETS_PATH . 'stylesheets/');
define('JAVASCRIPTS_PATH', ASSETS_PATH . 'javascripts/');
define('IMAGES_PATH', ASSETS_PATH . 'images/');
define('DO_PATH', 'do/');
define('AJAX_PATH', 'ajax/');

// SERVER
define('FPF_CONTROLLERS_DIR', FPF_BASE_DIR . 'controllers/');
define('FPF_LIBRARY_DIR', FPF_BASE_DIR . 'library/');
define('FPF_DAOS_DIR', FPF_BASE_DIR . 'daos/');
define('FPF_PLUGINS_DIR', FPF_BASE_DIR . 'plugins/');
define('FPF_MODELS_DIR', FPF_BASE_DIR . 'models/');
define('FPF_SCRIPTS_DIR', FPF_BASE_DIR . 'scripts/');
define('FPF_VIEWS_DIR', FPF_BASE_DIR . 'views/');
define('FPF_VIEWS_CONTENT_DIR', FPF_VIEWS_DIR . 'content/');
define('FPF_VIEWS_FRAME_DIR', FPF_VIEWS_DIR . 'frame/');
define('FPF_VIEWS_TEMPLATE_DIR', FPF_VIEWS_DIR . 'template/');
?>