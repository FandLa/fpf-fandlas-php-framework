<?php

/**
 * routes for mapping the site url to a specific controller and action
 */
$this->config['routes'] = array(

	// /logout/param1/...
	array('#^/logout(/)?(?(1)(.*))$#', function($match) {
		return new Request('Do' . FPF_CONTROLLER_SUFFIX, 'logout', explode('/', $match[0]));
	}),

	// /set-locale/param1/...
	array('#^/?set-locale(/)?(?(1)(.*))$#', function($match) {
		return new Request('Do' . FPF_CONTROLLER_SUFFIX, 'set_locale', explode('/', $match[0]));
	}),
	
	// /assets/
	array('#^/?' . ltrim(url(ASSETS_PATH), '/') . '#', function($match) {
		// redirect to root page if user tries to access the assets folder (does not apply to files inside the assets folder)
		redirect(url('/'));
	}),
	
	//	/<controller>/<action>/param1/param2/...
	array('#^/?([^/]+)/([^/]+)/?(.*)$#', function($match) {
		return new Request(ucfirst(preg_replace('#[^a-zA-Z0-9_]#', '_', strtolower($match[1]))) . FPF_CONTROLLER_SUFFIX, preg_replace('#[^a-zA-Z0-9_]#', '_', $match[2]), explode('/', $match[0]));
	}),
	
	// /<controller>/
	array('#^/?([^/]+)/?(.*)$#', function($match) {
		return new Request(ucfirst(preg_replace('#[^a-zA-Z0-9_]#', '_', strtolower($match[1]))) . FPF_CONTROLLER_SUFFIX, FPF_DEFAULT_ACTION, array());
	}),
	
	// /
	array('#^/?(.*)$#', function($match) {
		return new Request(FPF_DEFAULT_CONTROLLER, FPF_DEFAULT_ACTION, array());
	})

);

?>