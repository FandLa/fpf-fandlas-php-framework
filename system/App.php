<?php

require_once FPF_LIBRARY_DIR . '/FPF/BaseApp.php';

class App extends BaseApp {
	
	public function pre_process() {
		if($this->request->controller->requires_logged_user()) {
			if($this->userSession->getCurrentUser() == NULL) {
				if(!$this->request->controller->on_unlogged_user()) {
					return false;
				}
			}
		}
	}
	
	public function post_process() {
		
	}
	
}

?>