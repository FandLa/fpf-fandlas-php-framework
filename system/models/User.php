<?php

class User {
	
	private $id;
	private $name;
	private $encodedPassword;
	
	public function User($id, $name, $encodedPassword) {
		$this->id = $id;
		$this->name = $name;
		$this->encodedPassword = $encodedPassword;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getEncodedPassword() {
		return $this->encodedPassword;
	}
	
}
