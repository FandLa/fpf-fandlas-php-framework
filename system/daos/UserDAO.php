<?php

include_once FPF_MODELS_DIR . '/User.php';

class UserDAO extends DatabaseDAO {
	
	public function getTableName() {
		return 'user';
	}
	
	public function assocToModel($row) {
		return new User($row['id'], $row['name'], $row['encoded_password']);
	}
	
	public function insert($name, $encoded_password) {
		return $this->executeInsert(array('name' => $name, 'encoded_password' => $encoded_password));
	}
	
	public function update($id, $name, $password) {
		echo 'update: ' . $id . ' , ' .$name . ' , ' . $password . '<br />';
		return $this->executeUpdate(array('id' => $id), array('name' => $name, 'encoded_password' => encode($password)));
	}
	
	public function delete($id) {
		return $this->executeDelete(array('id' => $id));
	}
	
	public function getUserById($userId) {
		return $this->executeGet(array('id' => $userId));
	}
	
	public function getUserByName($name) {
		return $this->executeGet(array('name' => $name));
	}
	
	public function getUserByNameAndEncodedPassword($name, $encodedPassword) {
		return $this->executeGet(array('name' => $name, 'encoded_password' => $encodedPassword));
	}
	
	public function getUsers($limit = 0, $skip = 9999999999) {
		return $this->executeGetAll(NULL, $limit, $skip);
	}
	
}
