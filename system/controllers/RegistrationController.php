<?php

class RegistrationController extends Controller {
	
	public function requires_logged_user() {
		return false;
	}
	
	public function load_stylesheets() {
		return array('forms.css');
	}
	
	public function action_index($request) {
		if(isset($request->post->registration_submit)) {
			$userName = $request->post->registration_name;
			$userPassword = $request->post->registration_password;
			$userConfirmPassword = $request->post->registration_confirm_password;
			if($this->userSession->registrate($userName, $userPassword, $userConfirmPassword)) {
				$this->session->registration_feedback[] = L_REGISTRATION_SUCCESS;
			} else {
				$this->session->registration_feedback = $this->userSession->getRegistrationErrors();
			}
			// redirect_current();
		}
		$this->set_view('content', 'registration.php');
	}
	
}

?>