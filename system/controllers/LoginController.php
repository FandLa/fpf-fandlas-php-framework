<?php

class LoginController extends Controller {
	
	public function requires_logged_user() {
		return false;
	}
	
	public function load_stylesheets() {
		return array('forms.css');
	}
	
	public function action_index($request) {
		if(isset($request->post->login_submit)) {
			$userName = $request->post->login_name;
			$userPassword = $request->post->login_password;
			$userRememberMe = isset($request->post->login_remember);
			if($this->userSession->login($userName, $userPassword, $userRememberMe)) {
				redirect('/');
			} else {
				$this->session->login_feedback = $this->userSession->getLoginErrors();
				redirect_current();
			}
		}
		$this->set_view('content', 'login.php');
	}
	
}

?>