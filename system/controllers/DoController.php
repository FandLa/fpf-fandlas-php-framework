<?php

class DoController extends Controller {
	
	public function requires_session_token() {
		return true;
	}
	
	public function on_invalid_session_token() {
		echo 'Session expired.<br />If you are not redirected within a few seconds, click <a href="javascript: history.go(-1);">here</a>.';
		echo '<script>setTimeout(function() { history.go(-1); }, 5000);</script>';
		return false;
	}
	
	public function requires_logged_user() {
		return false;
	}
	
	/**
	 * Overriding the Controller's display() method prevents html output
	 */
	public function display() {
		return false;
	}
	
	public function action_undefined($request) {
		redirect();
		return false;
	}
	
	public function action_set_locale($request)  {
		if(isset($request->path[1])) {
			$locale = $request->path[1];
			if(in_array($locale, array('de', 'en'))) {
				$this->dictionary->set_locale($locale);
			}
		}
		if(isset($request->get->redirect)) {
			redirect_absolute($request->get->redirect);
		}
		return false;
	}
	
	public function action_logout($request) {
		$this->userSession->logout();
		redirect('/login');
		return false;
	}
	
	public function action_delete_user($request) {
		if(isset($request->get->{'user-id'})) {
			if(($user = UserDAO::instance()->getUserById($request->get->{'user-id'})) != NULL) {
				$this->session->deleteUserName = $user->getName();
				if(UserDAO::instance()->delete($request->get->{'user-id'})) {
					$this->session->deleteUserFeedback = L_DELETE_USER_SUCCESS;
				} else {
					$this->session->deleteUserFeedback = L_DELETE_USER_FAILURE;
				}
			} else {
				$this->session->deleteUserFedback = L_DELETE_USER_FAILURE;
			}
		}
		if(isset($request->get->redirect)) {
			redirect_absolute($request->get->redirect);
		}
		return false;
	}
	
}

?>