<?php

class ErrorController extends Controller {
	
	public function requires_logged_user() {
		return false;
	}
	
	public function action_index($request) {
		$this->view->controller_action = 'ErrorController::action_index';
		
		$this->view->message = '<h1>Requested page does not exist!</h1>';
		$this->set_view('content', 'error.php');
	}
	
}

?>