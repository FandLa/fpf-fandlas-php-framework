<?php

class AjaxController extends Controller {
	
	public function requires_ajax_token() {
		return true;
	}
	
	public function on_invalid_ajax_token() {
		echo 'Expired!';
		return false;
	}
	
	/**
	 * Overriding the Controller's display() method prevents html output
	 */
	public function display() {
		
	}
	
	public function action_echo($request) {
		echo $request->post->var;
	}
	
}

?>