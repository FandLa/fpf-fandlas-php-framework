<?php

class IndexController extends Controller {
	
	public function load_stylesheets() {
		return array('forms.css');
	}
	
	public function action_index($request) {
		$this->view->controller_action = 'IndexController::action_index';
		
		$this->view->page_title_message = text('welcome message', array('page_name' => 'FandLas PHP Framework'));
		
		$this->view->users = UserDAO::instance()->getUsers();
		
		$this->view->editUser = NULL;
		if(isset($request->get->task) && $request->get->task == 'edit' && isset($request->get->{'user-id'})) {
			$this->view->editUser = UserDAO::instance()->getUserById($request->get->{'user-id'});
		}
		
		if(isset($request->post->edit_user_submit)) {
			$editUserId = $request->post->edit_user_id;
			$editUserName = $request->post->edit_user_name;
			$editUserPassword = $request->post->edit_user_password;
			if(UserDAO::instance()->update($editUserId, $editUserName, $editUserPassword)) {
				$this->session->editUserFeedback = L_EDIT_USER_SUCCESS;
			} else {
				$this->session->editUserFeedback = L_EDIT_USER_FAILURE;
			}
			$this->session->editUserId = $editUserId;
			redirect_current();
		}
		
		$this->set_view('content', 'index.php');
	}
	
}

?>