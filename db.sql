DROP DATABASE IF EXISTS `game`;

CREATE DATABASE IF NOT EXISTS `game`;

USE `game`;

CREATE TABLE `user` (
	`id` int(255) PRIMARY KEY,
	`name` varchar(100) NOT NULL,
	`encoded_password` varchar(100) NOT NULL
);